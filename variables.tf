variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/terraform.pub
DESCRIPTION
  default   = "~/.ssh/terraform.pub"
}

variable "private_key_path" {
  default   = "~/.ssh/terraform"
}

variable "key_name" {
  description = "Desired name of AWS key pair"
  default   = "terraform-key-pair-syd"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-southeast-2"
}

# Ubuntu Precise 12.04 LTS (x64)
variable "aws_amis" {
  default = {
    ap-southeast-2 = "ami-41c12e23"
  }
}
